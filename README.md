# web-dev-code-review

A web form mockup written by Ian S. Pringle for a code review.
Task was to mimic an existing web form, adding responsive elements,
accessibility, and client-side input validation.

![screenshot](scrot.png "Screenshot")
