const addEvent = (element, kind, func) => {
  if (element.addEventListener) {
    element.addEventListener(
      kind,
      function(e) {
        func(e, e.target);
      },
      false
    );
  } else if (element.attachEvent) {
    element.attachEvent("on" + kind, function(e) {
      func(e, e.srcElement);
    });
  }
};

const showError = (element, message) => {
  let errorSpan = document.getElementsByClassName("error")[0];
  let error_name = element.id.replaceAll("_", " ");
  element.classList.add("invalid");
  errorSpan.innerHTML = "invalid " + error_name;
};

const validate = (element, value, pattern) => {
  if (pattern.test(value)) {
    element.classList.remove("invalid");
    return true;
  } else {
    showError(element, "Invalid!");
    return false;
  }
};

const form = document.getElementsByName("interest_form")[0];
addEvent(form, "change", function(e, t) {
  let pattern = / /;
  t.placeholder = "";
  switch (t.id) {
    case "address":
      // I dislike this pattern. I dislike using regexp for addresses, but the USPS site didn't want to
      // let me use their REST API and I didn't find any other free address validation service
      // These should be valid:
      // 1971 University Blvd. Lynchburg VA 24504
      // 1971 University Blvd., Lynchburg, Virginia, 24504
      // 1971A University Blvd., Lynchburg, Virginia, 24504
      // Doesn't cover PO Boxes
      pattern = /^(?:[0-9]+[\-]?[a-zA-Z]?) (?:[\w\.\- ]+),? (?:[\w\.\-]+),? (?:[A-Z|\w]+),? (?:[0-9]{5})$/;
      validate(t, t.value, pattern);
      break;

    case "telephone":
      // Modifying this from a contribution I made here: https://github.com/emilmerle/FindingThingsWithPython/blob/4a788211f6ecaf70c8e2d3cb08bb6501d078bf8f/phoneNumbers/countries.py#L7
      // This will only work for USA/Canada, but hey probably shouldn't be writing your own telephone number
      // validation regexp in the wild anyway!
      pattern = /^(?:1[- ]?)?(?:\(?(?:[2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\)?[- ]?)?(?:[2-9]1[02-9]|2-9][02-9]1|[2-9][02-9]{2})[- ]?\d{4}$/;
      validate(t, t.value, pattern);
      break;

    case "email":
      // This is the ietf standard for email https://tools.ietf.org/html/rfc3696#page-5
      // per their recommendation, only checking the very first char of the TLD, technically
      // a TLD can contain numerals, but we're just going to leave that off until someone
      // buys a TLD with numbers in it. I'd like to have added a `$` to anchor this down, but
      // it returns all emails as invalid then, not sure why that would be unless there isn't
      // a new line attached to the value.
      pattern = /^[a-zA-Z0-9!#$%&\'\*\+\-\_\?\^\`\.\{\|\}\~]+@+[a-zA-Z-0-9-]+(?:\.[a-zA-Z])+$/;
      validate(t, t.value, pattern);
      break;

    case "last_name":
    case "first_name":
      // Just don't let it be empty or whitespace
      pattern = /^(?:.|\s)*\S(?:.|\s)*/;
      validate(t, t.value, pattern);
      break;

    case "degree_type":
      document.getElementById("study_area").disabled = false;
      // TODO: make it so this actually works, currently it isn't updating
      document.getElementById("study_area").children[0].innerHTML = "Select";
      break;
  }
});

addEvent(form, "submit", function(e, t) {
  let valid = Object.values(t.elements).every(
    (part) => part.validity.valid === true
  );
  if (valid) {
    let form_box = document.getElementById("form_box");
    let after_submit =
      "We've recieved your request!<br/><button type='submit' class='full' id='submit_again' onclick=resetForm()>Submit again</button>";
    replaceContent(form_box, after_submit);
  } else {
    e.preventDefault();
  }
});

// Snapshot form for later
let form_box = document.getElementById("form_box");
let form_content = form_box.innerHTML;

const replaceContent = (container, content) => {
  container.innerHTML = content;
};

const resetForm = () => replaceContent(form_box, form_content);
